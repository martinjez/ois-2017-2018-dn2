// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':;
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
//##############################################################################
    case 'barva':
      
      besede.shift();
      var kateraBarva = besede.join(' ');
      document.querySelector("#sporocila").style.color = kateraBarva;
      document.querySelector("#kanal").style.color = kateraBarva;
      
      console.log("Spreemba barve!");
      break;
//##############################################################################
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      console.log("LOG par: " + parametri);
      console.log("LOG par1: " + parametri[1]);
      console.log("LOG par2: " + parametri[2]);
      console.log("LOG par3: " + parametri[3]);
      
      if (parametri) {
        
//##############################################################################
        if (parametri == undefined || parametri == null) {
          var uporabniki2 = parametri[1];
          console.log("Wahtis: " + uporabniki2);
        } else {
          var uporabniki2 = parametri[1].split(',');
          console.log("Waht igess: " + uporabniki2);
        }
       
        console.log("DEJ: " + uporabniki2);
        console.log("DEJ0: " + uporabniki2[0]);
        
        sporocilo = '(zasebno za ' + uporabniki2 + '): ' + parametri[3];
        
        for (var i in uporabniki2) {
          
          this.socket.emit('sporocilo', {vzdevek: uporabniki2[i], besedilo: parametri[3]});
            
        }

//******************************************************************************

      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  }

  return sporocilo;
};